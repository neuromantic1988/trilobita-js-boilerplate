/* jshint esversion:6 */
/*
 * Set main version of routers (API)
 */
const release = require('express').Router(),
	    testing = require('express').Router(),
	    development = require('express').Router(),
			/* Import interface with middlewares */
      middlewares = require('../interfaces/middlewares'),
			/* Import interface with answer builder */
      answerBuilder = require('../interfaces/answer-builder'),
			/* Import all getters */
      getters = require('../interfaces/getters'),
			/* Impoert all models */
      models = require('../interfaces/models'),
			/* Import all actions */
      actions = require('../interfaces/actions'),
			/* Impoert all utils */
      utils = require('../interfaces/utils');
/*
 * Add middlewares to all routes:
 * - filters;
 */
[
    release,
    testing,
    development
].forEach(rv => {
    rv.use(middlewares.getFilters);
});
/*
 * Root router
 */
development.route('/')
	  .get((req, res, next) => {
				/* If user call main API link - redirect
				* to about page*/
        res.redirect('/about');
    })
		.post((req, res, next) => {
				/* Answer, when method not supported */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'not-support'
				});
		})
		.put((req, res, next) => {
				/* Answer, when method not supported */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'not-support'
				});
		})
		.delete((req, res, next) => {
				/* Answer, when method not supported */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'not-support'
				});
		});
/*
 * Check health of microservice
 */
 development.route('/alive')
 	  .get((req, res, next) => {
					/* Answer, when micriseivice is alive */
					answerBuilder.state({
							res: res,
							req: req,
							state: 'i-am-alive'
					});
     })
 		.post((req, res, next) => {
				/* Answer, when micriseivice is alive */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'i-am-alive'
				});
 		})
 		.put((req, res, next) => {
				/* Answer, when micriseivice is alive */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'i-am-alive'
				});
 		})
 		.delete((req, res, next) => {
				/* Answer, when micriseivice is alive */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'i-am-alive'
				});
 		});
/*
 * Router to work with journals entry
 */
development.route('/entries')
    .get((req, res, next) => {
				/* Get all documents with type Entry */
        getters.findAll({
            model: models.entry,
            res: res,
            filters: req.filters,
            req: req,
            type: 'Entry'
        });
    })
    .post((req, res, next) => {
				/* Create new documents with type Entry */
        actions.saveEntry({
            req: req,
            res: res,
            model: models.entry
        });
    })
		.put((req, res, next) => {
				/* Answer, when method not supported */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'not-support'
				})
		})
    .delete((req, res, next) => {
				/* Remove document with type entry */
        actions.removeEntry({
            filters: utils.getFullFilters({
                filters: req.filters,
                default: {
                    key: '_id',
                    value: req.body.id
                }
            }),
            model: models.entry,
            res: res,
            req: req
        });
    });
/*
 * Work with one entry
 */
development.route('/entries/:id')
    .get((req, res, next) => {
				/* Get one document with type Entry
				* and ID from request params */
        getters.findOne({
            res: res,
            filters: utils.getFullFilters({
                filters: req.filters,
                default: {
                    key: '_id',
                    value: req.params.id
                }
            }),
            model: models.entry,
            req: req,
            type: 'Entry'
        });
    })
    .put((req, res, next) => {
				/* Update one document with type Entry
				* and ID from request params */
        actions.updateEntry({
            filters: utils.getFullFilters({
                filters: req.filters,
                default: {
                    key: '_id',
                    value: req.params.id
                }
            }),
            req: req,
            res: res,
            model: models.entry,
            type: 'Entry',
            update: req.body.update
        });
    })
		.delete((req, res, next) => {
				/* Answer, when method not supported */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'not-support'
				})
		})
		.post((req, res, next) => {
				/* Answer, when method not supported */
				answerBuilder.state({
						res: res,
						req: req,
						state: 'not-support'
				})
		});
/*
 * Export all routers
 */
module.exports = [
					{path: '/api/release', router: release},
					{path: '/api/development', router: development},
					{path: '/api/testing', router: testing}
];
