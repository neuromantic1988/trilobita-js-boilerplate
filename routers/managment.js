/* jshint esversion:6 */
/*
 * Set main version of routers (API)
 */
const release = require('express').Router(),
			testing = require('express').Router(),
			development = require('express').Router(),
			answerBuilder = require('../interfaces/answer-builder');
/*
 * Root router
 */
 development.route('/')
 	  .get((req, res, next) => {
 				/* If user call main API link - redirect
 				* to about page*/
         res.redirect('/about');
     })
 		.post((req, res, next) => {
 				/* Answer, when method not supported */
 				answerBuilder.state({
 						res: res,
 						req: req,
 						state: 'not-support'
 				});
 		})
 		.put((req, res, next) => {
 				/* Answer, when method not supported */
 				answerBuilder.state({
 						res: res,
 						req: req,
 						state: 'not-support'
 				});
 		})
 		.delete((req, res, next) => {
 				/* Answer, when method not supported */
 				answerBuilder.state({
 						res: res,
 						req: req,
 						state: 'not-support'
 				});
 		});
/*
 * Export all routers
 */
module.exports = [
					{path: '/managment/release', router: release},
					{path: '/managment/development', router: development},
					{path: '/managment/testing', router: testing}
				  ];
