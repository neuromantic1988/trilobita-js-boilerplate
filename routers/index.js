/* jshint esversion: 6*/

/*
 * Set main version of routers (server render)
 */

const router = require('express').Router(),
      middlewares = require('../interfaces/middlewares'),
      answerBuilder = require('../interfaces/answer-builder');


/*
 * Add middleware to all routers
 */
router.use(middlewares.getFilters);


/*
 * Index page (static)
 */
router.route('/')
    .get((req, res, next) => {
        /* Get render page */
        res.render('page-index');
    })
    .post((req, res, next) => {
        /* Answer, when method not supported */
        answerBuilder.state({
            res: res,
            req: req,
            state: 'not-support'
        });
    })
    .put((req, res, next) => {
        /* Answer, when method not supported */
        answerBuilder.state({
            res: res,
            req: req,
            state: 'not-support'
        });
    })
    .delete((req, res, next) => {
        /* Answer, when method not supported */
        answerBuilder.state({
            res: res,
            req: req,
            state: 'not-support'
        });
    });
/*
 * Module exports
 */
module.exports = {
    path: '/about',
    router: router
};
