/* jshint esversion:6 */

/*
 * This file contains access to all the
 * settings objects that are in the application.
 */
module.exports = {
    mongodb: require('../configs/mongodb-settings'),
    mongoexpress: require('../configs/mongo-express'),
    server: require('../configs/server-settings'),
    routersFiles: require('../configs/router-files')
};
