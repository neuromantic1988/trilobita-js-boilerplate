/* jshint esversion:6 */

/*
 * Module exports
 */
module.exports =  {
    /*
     * Fullfeature logger based on Winston, contains
     * two step: outppou in console & output in file/
     * Database (MongoDB).
     */
    full: require('../helpers/loggers/logger'),
    /* Logger for write text in console */
    console: require('../helpers/loggers/get-console-logger'),
    /* Logger for write text in file */
    file: require('../helpers/loggers/get-file-logger')
};
