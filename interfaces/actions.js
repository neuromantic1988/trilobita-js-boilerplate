/* jshint esversion:6 */
module.exports = {
    saveEntry: require('../helpers/actions/save-entry'),
    updateEntry: require('../helpers/actions/update-entry'),
    removeEntry: require('../helpers/actions/remove-entry')
};
