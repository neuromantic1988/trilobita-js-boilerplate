/* jshint */
module.exports = {
    /* Functional analog UNderscore */
    rambda: require('rambda'),
    /* Production ODM for MongoDB */
    mongoose: require('mongoose'),
    /* MongoDB admin panel based on ExpressJS */
    mongoExpress: require('mongo-express/lib/middleware'),
    /* Elders web-framework for NodeJS*/
    express: require('express'),
    /* Main router object */
    router: require('express').Router(),
    /* Module for parsing request body */
    bodyParser: require('body-parser'),
    /* Module for work with filesystem */
    fs: require('fs'),
    /* Module for worj with path in filesystem */
    path: require('path'),
    /* Fucntion for parsing cookies */
    cookieParser: require('cookie-parser'),
    /* Module for http server */
    http: require('http')
};
