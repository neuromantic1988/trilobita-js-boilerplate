/* jshint esversion: 6 */
module.exports = {
    cors: require('../middlewares/cors'),
    getFilters: require('../middlewares/get-filters'),
    mongoExpress: require('mongo-express/lib/middleware'),
    scrapyardInjection: require('../middlewares/scrapyard-injection')
};
