/* jsgint esversion:6 */
module.exports = {
    /* JSON API answer builder for states*/
    state: require('../helpers/json-builder/answer-builder').buildState,
    /* JSON API answer builder for errors */
    error: require('../helpers/json-builder/answer-builder').buildError,
    /* JSON API answer builder for data */
    data: require('../helpers/json-builder/answer-builder').buildData
};
