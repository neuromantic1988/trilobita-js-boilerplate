/* jshint esversion:6 */
module.exports = {
    findAll: require('../helpers/getters/find-all'),
    findOne: require('../helpers/getters/find-one')
	};
