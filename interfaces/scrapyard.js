/* jshint esversion:6 */

/*
 * A scrapyard object is an object that stores all globally variable application variables.
 * By flipping these objects - you can influence these or other parameters of the application,
 * as well as transfer links to such things as a connection to the database and work with it
 * from any place.
 */

/*
 * Module exports main global object
 */
module.exports = {
    httpServer: null,
    application: null,
    connection: {
        mainDatabse: null,
        cacheDatabase: null
    }
};
