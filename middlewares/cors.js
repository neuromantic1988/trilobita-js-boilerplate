/* jshint esversion:6 */
/*
 * Module exports
 */
module.exports = (req, res, next) => {
    /* Set headers for detect language */
    req.lang = (req.headers['x-language']) ? req.headers['x-language'].toLowerCase() : 'en';
    /* Some standart headers */
    res.header('Access-Control-Allow-Origin', '*');
    /* Allowed headers */
    res.header('Access-Control-Allow-Headers', 'referrer, x-access-token, Origin, X-Requested-With, Content-Type, Accept, x-language');
    /* Allowed methods */
    res.header('Access-Control-Allow-Methods', 'GET, POST, HEAD, PUT, DELETE, OPTIONS');
    /* Answer when mthod is options */
    if(req.method === 'OPTIONS') {
        res.status(200).json({});
    } else {
        next();
    }
};
