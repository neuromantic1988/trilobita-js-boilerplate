/* jshint esversion:6 */

/*
 * Module exports
 */
module.exports = scrapyard => {
    return (req, res, next) => {
        req.scrapyard = scrapyard;
        next();
    };
};
