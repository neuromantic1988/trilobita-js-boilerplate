/* jshint es-verstion */
/*
 * Module exports
 */
module.exports = (req, res, next) => {
    /* For kill bugs in the begining*/
    if(req.query === undefined) req.query = {};
    if(req.body === undefined) req.body = {};
    /* If query have prop fliters */
    if(req.query.hasOwnProperty('filters') === true) {
        req.filters = JSON.parse(req.query.filters);
        console.log(req.filters);
    } else {
        /* If body have prop filters */
        if(req.body.hasOwnProperty('filters') === true) {
            req.filters = req.body.filters;
        } else {
            req.filters = {};
        }
    }
    /* Go away */
    next();
};
