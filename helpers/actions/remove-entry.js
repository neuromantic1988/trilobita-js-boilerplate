/* jshint esversion: 6 */
const answerBuilder = require('../../interfaces/answer-builder');
/*
 * Module exports
 */
module.exports = obj => {
    obj.model.remove(obj.filters)
        .then(() => {
            /* If entry is removed - get answer
             * from state */
            answerBuilder.state({
                req: obj.req,
                res: obj.res,
                state: 'successfully-removed'
            });
        })
        .catch(error => {
            /* If system can not removed entry -
             * get answer with error*/
            answerBuilder.error({
                res: obj.res,
                req: obj.req,
                error: error
            });
        });
};
