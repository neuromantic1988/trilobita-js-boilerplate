/* jshint esversion:6 */
const answerBuilder = require('../../interfaces/answer-builder');
/*
  * Module exports
 */
module.exports = obj => {
    obj.model.findOne(obj.filters)
        .then(result => {
            if (result === null) {
                /* If resultat equal null */
                answerBuilder.data({
                    req: obj.req,
                    res: obj.res,
                    data: result,
                    type: obj.type || 'Undefined'
                });
            } else {
                /* Set mutable variable */
                let iterator = 1;
                /* Set immmutable variable */
                const lst = Object.keys(obj.update),
                      max = lst.length;
                /* iterate object fields on keys */
                lst.forEach(item => {
                    result[item] = obj.update[item];

                    if (max === iterator) {
                        result.__v = result.__v + 1;
                        result.save()
                            .then(data => {
                                /* Answer, when data saved */
                                answerBuilder.data({
                                    req: obj.req,
                                    res: obj.res,
                                    data: data,
                                    type: obj.type || 'Undefined'
                                });
                            })
                            .catch(error => {
                                /* Answer when data not saved */
                                answerBuilder.error({
                                    req: obj.req,
                                    res: obj.res,
                                    error: error
                                });
                            });
                    } else {
                        iterator += 1;
                    }
                });
            }
        })
        .catch(error => {
            /* Answer when error */
            answerBuilder.error({
                req: obj.req,
                res: obj.res,
                error: error
            });
        });

};
