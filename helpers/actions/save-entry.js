/* jshint esversion: 6 */
const answerBuilder = require('../../interfaces/answer-builder');

module.exports = obj => {
    console.log(obj.model);
    /* Create new mongoose model */
	  new obj.model(obj.req.body.newObj).save()
		    .then(resultat => {
            /* Get answer with success */
            answerBuilder.state({
                res: obj.res,
                req: obj.req,
                state: 'successfully-created'
            });
		    })
		    .catch(error => {
            /* Get answer with error */
			      answerBuilder.error({
                res: obj.res,
                req: obj.req,
                error: error
            });
		    });
};
