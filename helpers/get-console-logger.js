/* jshint esversion:6 */
const winston = require('winston'),
  consoleLogger = new(winston.Logger)({
    transports: [
      new winston.transports.Console({
        colorize:   true,
        label:      'server'
      })
    ]
  });
// Module exports
module.exports = consoleLogger;
