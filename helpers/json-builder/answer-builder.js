const states = require('./states'),
      logger = require('../../helpers/loggers/logger'),
      buildError = obj => {
          /**
           * Build JSON based on JS error object.
           * obj.res
           * obj.req
           * obj.error
           * obj.logger (true/false)
           */
          if (states[obj.error.name] === undefined) {
              if (obj.error.name == 'MongoError') {
                  switch (obj.error.code) {
                  case 11000:
                      obj.error.name = 'already-exist';
                      break;
                  }
              } else if (obj.error.name == 'ValidationError') {
                  obj.error.name = 'validation-error';
              } else {
                  logger({
                      title: 'Undefined error',
                      status: 500,
                      message: obj.error
                  });
                  obj.error.name = 'internal-error';
              }
          }
          let json = {
              errors: [{
                  code: states[obj.error.name]['code'],
                  detail: states[obj.error.name]['detail'][obj.req.lang]
              }]
          };
          http_status = parseInt(states[obj.error.name]['code'].split('-')[0]) || 500;
          obj.res.status(http_status).json(json);
      },
      buildState = obj => {
          /**
           * Build JSON based on JS error object.
           * obj.res
           * obj.req
           * obj.state
           */
          if (states[obj.state] === undefined) {
              logger({
                  title: 'Undefined state',
                  status: 500,
                  message: 'Not specified state usage'
              });
              obj.state = 'internal-error';
          }
          let http_status = parseInt(states[obj.state]['code'].split('-')[0]) || 500;
          let json = {};
          if (http_status < 300) {
              json = {
                  meta: [{
                      code: states[obj.state]['code'],
                      detail: states[obj.state]['detail'][obj.req.lang]
                  }]
              };
          } else {
              json = {
                  errors: [{
                      code: states[obj.state]['code'],
                      detail: states[obj.state]['detail'][obj.req.lang]
                  }]
              };
          }
          if (obj.req.metaToken) {
              json.meta = {
                  token: obj.req.metaToken
              };
          }
          obj.res.status(http_status).json(json);
      },
      buildData = obj => {
          console.log('MY DARTA', obj.data);
          /**
           * Build JSON based on JS error object.
           * obj.res
           * obj.req
           * obj.data
           * obj.type
           * obj.id
           */
          let json = {},
              http_status = 200;
          if (Array.isArray(obj.data) === true) {
              json.data = [];
              obj.data.forEach((element) => {
                  json.data.push({
                      id: obj.id || element._id || element.id,
                      type: obj.type,
                      attributes: element
                  });
              });
          } else {
              if (obj.data === null || obj.data.length === 0 || Object.keys(obj.data) === 0) {
                  http_status = 404;
                  json = {};
                  json.errors = [{
                      code: '404',
                      detail: states['not-found'][obj.req.lang] + ': ' + obj.type
                  }];
              } else {
                  json.data = {
                      type: obj.type,
                      id: obj.id || obj.data._id || obj.data.id,
                      attributes: obj.data
                  };
              }
          }
          if (obj.req.metaToken) {
              json.meta = {
                  token: obj.req.metaToken
              };
          }
          if (obj.req.meta || obj.meta) {
              json.meta = obj.req.meta || obj.meta;
          }
          obj.res.status(http_status).json(json);
      };
/*
 * Module exports
 */
module.exports = {
    buildError: buildError,
    buildData: buildData,
    buildState: buildState
}
