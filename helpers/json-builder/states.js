/**
 * use ISO-639-1 for languages
 * sort this states by code for readability
 */
module.exports = {
    'successfully-created': {
        'code': '200-5',
        'detail': {
            'en': 'Document successuly saved!',
            'ru': 'Документ успешно сохранен'
        }
    },
    'successfully-removed': {
        'code': '200-6',
        'detail': {
            'en': 'You have succesfully removed documents',
            'ru': 'Вы успешно удалили документ из системы'
        }
    },
    'i-am-alive': {
        'code': '200-11',
        'detail': {
            'en': 'This microservice is alive!',
            'ru': 'Данный микросервис жив!'
        }
    },
    'bad-request': {
        'code': '400',
        'detail': {
            'en': 'Wrong request. There is no necessary fields',
            'ru': 'Неверный запрос. Отсутствуют обязательные данные'
        }
    },
    'already-exist': {
        'code': '400-7',
        'detail': {
            'en': 'Document already exist',
            'ru': 'Запись уже существует'
        }
    },
    'validation-error': {
        'code': '400-8',
        'detail': {
            'en': 'Bad request. Validation error',
            'ru': 'Неверный запрос. Ошибка валидации'
        }
    },
    'TokenExpiredError': {
        'code': '403-1',
        'detail': {
            'en': 'Token is expired',
            'ru': 'Токен просрочен'
        }
    },
    'JsonWebTokenError': {
        'code': '403-2',
        'detail': {
            'en': 'Token is not valid',
            'ru': 'Некорректный токен'
        }
    },
    'not-finded-role': {
        'code': '403-4',
        'detail': {
            'en': 'Role not found',
            'ru': 'Роль не найдена'
        }
    },
    'access-denied': {
        'code': '403-5',
        'detail': {
            'en': 'Access denied',
            'ru': 'Доступ запрещен'
        }
    },
    'not-found': {
        'code': '404',
        'detail': {
            'en': 'Not found',
            'ru': 'Не найдено'
        }
    },
    'not-support': {
        'code': '405',
        'detail': {
            'en': 'Method not allowed',
            'ru': 'Метод не поддерживается'
        }
    },
    'specify-request': {
        'code': '449',
        'detail': {
            'en': 'Please specify request',
            'ru': 'Пожалуйста, уточните запрос'
        }
    },
    'internal-error': { // required default state
        'code': '500',
        'detail': {
            'en': 'DB unknown error. Try again later',
            'ru': 'Незвестная ошибка базы данных. Попробуйте позднее'
        }
    }
}
