/* jshint esversion:6 */
const answerTemplate = {
		data: {},
		errors: {},
		meta: {}
	},
	fullJSON = {
		status: 200,
		json: {}
	},
	clearTemplate = () => {
		answerTemplate.data = {};
		answerTemplate.errors = {};
		answerTemplate.meta = {};
	},
	errorDecoded = (error) => {
		answerTemplate.errors = {
			status: 'access-denied',
			message: 'Can not decode token'
		};
		answerTemplate.meta = error;
		fullJSON.status = 404;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
	blackListResource = () => {
		answerTemplate.errors = {
			status: 'access-denied',
			message: 'Can not find resource with this token'
		};
		answerTemplate.meta = {
			description: 'This source in black list'
		};
		fullJSON.status = 404;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
	tokenNotSend = () => {
		answerTemplate.errors = {
			status: 'token-not-send',
			message: 'Token not send with request!'
		};
		fullJSON.status = 404;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
	nothingFound = (obj) => {
		answerTemplate.errors = {
			type: obj.type,
			message: obj.message || `Application can not find document(s) with type ${obj.type}`
		};
		if (Array.isArray(obj.exhaust)) answerTemplate.data = [];
		fullJSON.status = 404;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
	giveDocument = (obj) => {
		answerTemplate.data = {
			id: obj.exhaust._id,
			type: obj.type,
			attributes: obj.exhaust
		};
		if (obj.hasOwnProperty('meta') === true) answerTemplate.meta = obj.meta;
		fullJSON.status = 200;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
	giveDocuments = (obj) => {
		let max = obj.data.exhaust.length,
			objs = [],
			iterator = 1;
		obj.exhaust.forEach(item => {
			objs.push({
				id: obj._id,
				type: objs.type,
				attributes: item
			});
			if (max !== iterator) {
				iterator += 1;
			} else {
				answerTemplate.data = objs;
				answerTemplate.meta.count = max;
				fullJSON.status = 200;
				fullJSON.json = answerTemplate;
			}
		});
		return fullJSON;
	},
	databaseError = (obj) => {
		answerTemplate.data = {};
		answerTemplate.errors = {
			error: `${obj.error.body}`,
			status: obj.error.type
		};
		fullJSON.status = 501;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
	successSaved = (obj) => {
		answerTemplate.data = {
			status: 'success',
			message: `Entry with type ${obj.type} succefully saved`
		};
		answerTemplate.meta = {
			resultat: obj.obj
		};
		fullJSON.status = 200;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
  successRemoved = (obj) => {
      answerTemplate.data = {
          status: 'success',
          message: `Entry with type ${obj.type} succefully removed`
      };
      answerTemplate.meta = {
          result: obj
      };
      fullJSON.status = 200;
      fullJSON.json = answerTemplate;
      return fullJSON;
  },
	buildError = (obj) => {
		switch (obj.error.type) {
			case 'decoded':
				return errorDecoded(obj.error.body);
			case 'database-error':
				return databaseError(obj);
			case 'blocked':
				return blackListResource();
			case 'token-not-send':
				return tokenNotSend();
			default:
				return buildJSON(obj);
		}
	},
	unknownAnswer = () => {
		answerTemplate.data = {
			status: 'unknown',
			message: 'This message send by unknown action!'
		};
		fullJSON.status = 404;
		fullJSON.json = answerTemplate;
		return fullJSON;
	},
  welcomeMessage = () => {
      answerTemplate.data = {
          message: 'Welcome to ope API AxolotlJS'
      };
      fullJSON.status = 200;
      fullJSON.json = answerTemplate;
      return fullJSON;
  },
	buildStateAnswer = (obj) => {
		  switch (obj.state) {
      case 'success-saved':
				  return successSaved(obj);
      case 'success-removed':
          return successRemoved(obj);
      case 'welcome':
          return welcomeMessage();
			default:
				return unknownAnswer();
		}
	},
  buildJSON = (obj) => {
		if (Array.isArray(obj.exhaust === true &&
				obj.exhaust.length !== 0)) {
			return giveDocuments(obj);
		} else {
        if (obj.exhaust === null ||
            obj.exhaust.length === 0 ||
            Object.keys(obj.exhaust) === 0) {
            return nothingFound(obj);
        } else {
            return giveDocument(obj);
        }
		}
	},
	buildAnswer = (obj) => {
                if(obj.hasOwnProperty('state') === true) {
                        return buildStateAnswer(obj);
                } else {
                        if (obj.hasOwnProperty('error') === true) {
                                return buildError(obj);
                        } else {
                                return buildJSON(obj);
                        }
                }
	},
	getAnswer = (obj) => {
		  clearTemplate();
		  let currentAnswer = buildAnswer(obj.data);
		  obj.res.status(currentAnswer.status).json(currentAnswer.json);
	};
/* Module exports */
module.exports = getAnswer;
