/* jshint esversion:6 */
module.exports = (obj) => {
    const tmp = {};
    tmp[obj.default.key] = obj.default.value;

    if(Object.keys(obj.filters).length !== 0) {
        Object.keys(obj.filters).forEach(key => {
            tmp[key] = obj.filters[key];
        });
    }
    return tmp;
};
