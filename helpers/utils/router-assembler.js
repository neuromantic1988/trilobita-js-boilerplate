/* jshint esversion:6 */
/* function small builder for work with arrays*/
const loggers = require('../../interfaces/loggers'),
      settings = require('../../interfaces/settings'),
      smallArrayBuilder = obj => {
          obj.elements.forEach(router => {
              obj.application.use(router.path, router.router);
              loggers.console.info(`Build router ${obj.fileName} => ${router.path}`);
          });
      },
      /* Function small builder for work with object */
      smallObjectBuilder = obj => {
          obj.application.use(obj.element.path, obj.element.router);
          loggers.console.info(`Build router ${obj.fileName} => ${obj.element.path}`);
      };
/*
 * Module exports
 */
module.exports = obj => {
    /* Set immutable variables */
    const fileList = settings.routersFiles,
          max = fileList.length;
    /* Set mutable variables */
    let currentFileData = null,
        iterator = 0;
    /* Check length of file list */
    if(max === 0) {
        /* If application folder have not files with routers -
         * write message on console and database/file*/
        loggers.console.error('Please add routers list to application');
        loggers.full({
            title: 'File list is empty',
            status: 'failed-buildB',
            message: 'Applications have not routers'
        });
     } else {
        /* Iterate all file list */
         fileList.forEach(fileName => {
            /* Check iteration number */
            if(max > iterator) {
                /* Get data from file */
                currentFileData = require(`../../routers/${fileName}`);
                /* Check type requred data */
                if(Array.isArray(currentFileData) === true) {
                    /* If type array - call small builder for array */
                    smallArrayBuilder({
                        application: obj.app,
                        elements: currentFileData,
                        fileName: fileName
                    });
                } else {
                    /* If type of data - not array - call small builder for object */
                    smallObjectBuilder({
                        application: obj.app,
                        element: currentFileData,
                        fileName: fileName
                    });
                }
            } else {
                /* Write messge on console */
                loggers.console.info('End router builds...');
            }
         });
     }
};
