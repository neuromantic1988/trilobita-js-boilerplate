/* jshint esversion:6
 * Connections function for MongoDB,
 * user Scrapyard variable connection/mainDatabase
 */
const loggers = require('../../interfaces/loggers'),
      modules = require('../../interfaces/modules'),
      settings = require('../../interfaces/settings'),
      successConnection = obj => {
          loggers.console.info('Database are connected!');
      },
      failedConnection = obj => {
          loggers.console.info(`Error database connection! Message: ${obj.error}`);
          loggers.full({
              title: 'Can not connection!',
              status: 'failed connection',
              message: obj.error
          });
      },
      connect = obj => {
          /* Write message on console */
          loggers.console.info('Try connnection to MongoDB with applications settongs...');
          /* Set native promice for Mongoose ODM */
          loggers.console.info('Chenge mongoose.Promise to native V8 Promise...');
          modules.mongoose.Promise = global.Promise;
          /* Variation based on settings */
          if (settings.mongodb.hasOwnProperty('user') === true &&
              settings.mongodb.hasOwnProperty('password') === true) {
              loggers.console.info('User & passwerd to MongoDB set in settings!');
              /* If settings have 'user' & 'password' keys
               * set uri staing with this data */
              const uri = `mongodb://${settings.mongodb.host || 'localhost'}:${settings.mongodb.port || '27017'}/${settings.mongodb.database || 'test'}`;
              /* Connect to MongoDB from Mongoose */
              obj.connection.mainDatabase = modules.mongoose.connect(uri, obj.options)
                  .then(() => {
                      successConnection(obj);
                  })
                  .catch(error => {
                      failedConnection({
                          error: error
                      });
                  });
          } else {
              loggers.console.warn('User & password to MongoDB not set in settings!');
              /* If settings databse haven't keys 'user' & 'password'*/
              obj.connection.mainDatabase  = modules.mongoose.connect(`${settings.mongodb.host || 'localhost'}:${settings.mongodb.port || '27017'}/${settings.mongodb.database || 'test'}`)
                  .then(() => {
                      loggers.console.info('Database is connected!');
                  })
                  .catch(error => {
                      failedConnection({
                          error: error
                      });
                  });
          }
      },
    /* Functon of disconnecting */
      disconnect = obj => {
          obj.connection.mainDatabase.disconnect();
      };

/*
 * Module exports
 */
module.exports = {
    connect: connect,
    disconnect: disconnect
};
