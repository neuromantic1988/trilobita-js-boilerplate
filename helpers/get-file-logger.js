/* jshint esversion:6 */
const winston = require('winston'),
	date = new Date(),
	fileLogger = new(winston.Logger)({
			transports: [
				new winston.transports.File({
						filename: `log-${date.getFullYear()}-${date.getMonth()}-${date.getDay()}.log`,
						level: 'error'
					})
			]
		});
/* Module exports */
module.exports = fileLogger;