/* jshint esversion:6 */
const winston = require('winston'),
  consoleLogger = new (winston.Logger)({
    transports: [
      new winston.transports.Console({
        colorize: true,
        level: 'debug'
      })
    ]
  });
module.exports = consoleLogger;
