/* jshint esversion:6 */
const fileLogger = require('../loggers/get-file-logger'),
  consoleLogger = require('../loggers/get-console-logger'),
  Entry = require('../../models/entry'),
  appLogger = obj => {
    new Entry({
      title: obj.title || 'Undefined title',
      status: obj.status || 'undefined error status',
      message: obj.message || 'Undefined error from server',
    }).save()
      .then(done => {
        if (obj.status == 200) {
          consoleLogger.info('<SERVER>: Entry saved in mongodb');
        } else {
          consoleLogger.error('<SERVER>: Entry saved in mongodb');
        }
      })
      .catch(error => {
        fileLogger.error(error);
      });
  };

module.exports = appLogger;