/* jshint esversion:6 */
const winston = require('winston'),
      fileLogger = new(winston.Logger)({
            transports: [
                new winston.transports.File({ filename: 'filelog-error.log', level: 'error' }),
            ]
      });
// Export module
module.exports = fileLogger; 
