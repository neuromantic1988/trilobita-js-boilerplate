/* jshint esversion:6 */
const consoleLogger = require('../helpers/get-console-logger'),
	  fileLogger = require('../helpers/get-file-logger'),
	  Entry = require('../models/entry'),
	  logger = obj => {
			new Entry(obj).save()
				.then(() => {
						consoleLogger.info('Log entry saved in database');
					})
				.catch(error => {
						consoleLogger.error(`When an application tries to write the document to the database An unexpected error occurred: ${error}`);
						fileLogger(obj);
					});
		};
/* Modile exports */
module.exports = logger;