/* jshint esversion:6 */
const answerBuilder = require('../../interfaces/answer-builder');
/*
 * Module exports
 */
module.exports = obj => {
		obj.model.findOne(obj.filters)
			  .then(resultat => {
					  answerBuilder.data({
							  res: obj.res,
                req: obj.req,
							  data:  resultat,
                type: obj.type
						});
				})
			.catch(error => {
					answerBuilder.error({
							res: obj.res,
							error: error,
              req: obj.req
						});
				});
	};
