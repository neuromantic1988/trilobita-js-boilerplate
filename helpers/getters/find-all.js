/* jshint esversion:6 */
const answerBuilder = require('../../interfaces/answer-builder');
/*
 * Module exports
 */
module.exports = (obj) => {
    /* Create immutable filter object */
    const filter = {};
    /* if object gabe filter key  */
    if(obj.filters) {
        /* Get all keys */
        const modelProps = Object.keys(obj.model.schema.paths);
        /* Iterate keys */
        for(let prop in obj.filters) {
            if(modelProps.indexOf(prop) > -1) {
                filter[prop] = obj.filters[prop];
            }
        }
    }
    /* Find all objects in MongoDB and send result */
    obj.model.find(filter)
		    .then(list => {
            /* Get answer with  data */
			      answerBuilder.data({
				        res: obj.res,
                req: obj.req,
				        data: list,
                type: obj.type
			      });
		    })
		    .catch(error => {
            /* Get answer with error */
			      answerBuilder.error({
				        res: obj.res,
                req: obj.req,
				        error: error
			      });
		});
};
