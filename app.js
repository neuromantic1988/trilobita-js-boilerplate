/* jshint esversion:6 */


/*
 * Import interfaces with main
 * functions.
 * Import modules native/nodejs
 */
const modules = require('./interfaces/modules'),
      /* Import logger helpers */
      loggers = require('./interfaces/loggers'),
      /* Import utils helpers*/
      utils = require('./interfaces/utils'),
      /* Import middlewares */
      middlewares = require('./interfaces/middlewares'),
      /* Import settings */
      settings = require('./interfaces/settings'),
      /* Set log directory path */
      logDirectory = modules.path.join(__dirname, 'log'),
      /* Set template directory path*/
      templateDirectory = modules.path.join(__dirname, 'views'),
/*
 * Main function fo app.js file (main application listing)
 * contains function 'getApplication', it get variable obj,
 * - object with main key/values prop. It can be used for
 * customization run of application.
 */
      getAppliaction = obj => {
          /*
           * Get express instance
           */
          getExpressInstance(obj.scrapyard);
          /*
           * Check for the folder 'logs'.
           * If it does not exist - we create it.
           */
          checkOrCreateLogDirectory();
          /*
           * View engine setup. If service not need server
           * template rendering execute command:
           * $: npm uninstall pug
           * After thet remove folder 'views'
           */
          setTemplateEngine({
              name: 'pug',
              scrapyard: obj.scrapyard
          });
          /*
           * Add to project common middlewares
           * (writen )
           */
          addMidlewaresToApp({
              list: [
                  'cors',
                  'getFilters'
              ],
              scrapyard: obj.scrapyard
          });
          /*
           * Add ascrapyard injection
           */
          obj.scrapyard.application.use(middlewares.scrapyardInjection(obj.scrapyard));
          /*
           * Connect to MongoDB
           */
          getDatabaseConnection(obj.scrapyard);
          /*
           * Run Mongo-Express
           */
          runMongoExpress({
              path: 'trilobita-admin',
              scrapyard: obj.scrapyard
          });
          /*
           * Run request body parser
           */
          addBodyParser({
              extended: false,
              scrapyard: obj.scrapyard
          });
          /*
           * Run request cookies parser
           */
          addCookiesParser({
              scrapyard: obj.scrapyard
          });
          /*
           * Add static folder
           */
          addStaticFolder({
              name: 'public',
              scrapyard: obj.scrapyard
          });
          /*
           * Add dynamic all routers to /api
           * and /managment routers.
           */
          routerAssembler(obj);
          /*
           * Middlewares for handler errors
           */
          obj.scrapyard.application.use(catchErrorHandler);
          obj.scrapyard.application.use(errorHandler);
          /*
           * Return application object
           */
          return obj.scrapyard.application;
      },
      /* Apllication object instance */
      getExpressInstance = obj => {
          /* Write message in console */
          loggers.console.info('Create application instance...');
          /* Set application object instance */
          obj.application = modules.express();
          /* Write message in console */
          loggers.console.info('Application instance has created!');
      },
      checkOrCreateLogDirectory = obj => {
          /* Write message on console */
          loggers.console.info('Check folder for saving logs... ');
          /* Check exist folder */
          if(modules.fs.existsSync(logDirectory)) {
              loggers.console.info('Log saving folder alredy exist!');
          } else{
              /* If folder not exist - create id
               * and write message on console */
              loggers.console.warn('Folder for store log-files not exist. Creating...');
              modules.fs.mkdirSync(logDirectory);
              loggers.console.info('Folder for store log-files are created!');
          }
      },
      /* Function check exist directory with templates &
       * if need - created at. */
      checkOrCreateTemplateDirectory = obj => {
          /* Write message on console */
          loggers.console.info('Check folder for store tamplates');
          /* Check exist folder */
          if(modules.fs.existSync(templateDirectory)) {
              /* If folder exist - wire message on console */
              loggers.console.info('Folder for store templates alredy exist!');
          } else {
              /* If folder not exist - create it */
              loggers.console.warn('Folder for store template not exist! Creating...');
              modules.fs.mkdirSync(templateDirectory);
              loggers.console.info('Folder for store templates are creating!');
          }
      },
      /* Function add to application folder with templates
       * and choose obj.name engine as default to use
       */
      setTemplateEngine = obj => {
          /* Write message on console */
          loggers.console.info('Setup template engine:');
          /* Add folder to application */
          obj.scrapyard.application.set('views', templateDirectory);
          /* Write message on console */
          loggers.console.info(`Set as template engin ${obj.name}...`);
          /* Add template engine to application */
          obj.scrapyard.application.set('view engine', obj.name);
      },
      /* Add custom middlewares to application */
      addMidlewaresToApp = obj => {
          /* Write message on console */
          loggers.console.info(`Add ${obj.list.length} middlewares to Express Applicatoion.`);
          /* Iterate list of middlewares
           * write message & add middleware to app */
          obj.list.forEach(name => {
              loggers.console.info(`Add to application middleware with name ${name}`);
              obj.scrapyard.application.use(middlewares[name]);
          });
      },
      /* Connect to database */
      getDatabaseConnection = obj => {
          /* Call connection function */
          utils.connectToMongo.connect(obj);
      },
      /* Function to run MongoExpress */
      runMongoExpress = obj => {
          /* Write message on console */
          loggers.console.info('Run MongoExpres...');
          /* Add MongoExpress middleware to application */
          obj.scrapyard.application.use(`/${obj.path}`, middlewares.mongoExpress(settings.mongoexpress));
      },
      /* Add body-parser middleware to application */
      addBodyParser = obj => {
          /* Write message on console */
          loggers.console.info('Add parser of body request to application...');
          /* Add body-parser to application */
          obj.scrapyard.application.use(modules.bodyParser.json());
          obj.scrapyard.application.use(modules.bodyParser.urlencoded({
              extended: obj.extended
          }));
      },
      /* Add cookie-parser to application */
      addCookiesParser = obj => {
          /* Write message on console */
          loggers.console.info('Add parser of cookies request to application...');
          /* Add cookies-parser to application */
          obj.scrapyard.application.use(modules.cookieParser());
      },
      /* Add static folder to application */
      addStaticFolder = obj => {
          /* Write messate on console */
          loggers.console.info(`Add access to public folder ${obj.name}...`);
          /* Add access to static folder from world */
          obj.scrapyard.application.use(modules.express.static(modules.path.join(__dirname, `${obj.name}`)));
      },
      /* Functoin to add routers to applications */
      routerAssembler = obj => {
          /* Write message on console */
          loggers.console.info('Atention! Builde application routers!');
          utils.routerAssembler({
              app: obj.scrapyard.application
          });
      },
      catchErrorHandler = (req, res, mext) => {
          const error = new Error('No found');

          error.status = 404;

          loggers.full({
              title: 'Not found',
              status: error.status,
              message: error
          });
      },
      errorHandler = (error, req, res, next) => {
          res.locals.message = error.message;
          res.locals.error = (req.app.get('env') === 'development') ? error : {};
          /*
           * Render error answer<0;48;47m[
           */
          res.status(error.status || 500);
          res.json({
              data: {},
              errors: {
                  status: error.status || 500,
                  message: error.message
              },
              meta: {}
          });
      };
/*
 * Module exports, function return application
 */
module.exports = getAppliaction;
