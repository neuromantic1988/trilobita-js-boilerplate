/**
  * @api {get} https://localhost:3030/about About page
  * @apiName About
  * @apiGroup General
  * @apiVersion 0.1.0
  * @apiDescription Main page - in this page you can see all libraries, used in Trilobita-JS.
  **/
  /**
    * @api {get} https://localhost:3030/api/development/entries Check health
    * @apiName Alive
    * @apiGroup General
    * @apiVersion 0.1.0
    *
    * @apiDescription Function to get answer, if microservice is alive
    *
    * @apiPermission All users
    *
    * @apiSuccessExample Success-Response:
    * HTTP 200 OK
    * {
	  *    "meta": [
		*               {
		*                 "code": "200-11",
		*                 "detail": "This microservice is alive!"
		*               }
		*     ]
    * }
    **/
  /**
    * @api {get} https://localhost:3030/api/development/entries Get entries
    * @apiName Entries
    * @apiGroup List arrays
    * @apiVersion 0.1.0
    *
    * @apiDescription Function to get all entries in server
    *
    * @apiPermission All users
    *
    * @apiSuccess {Array} data Object with resource params
    * @apiSuccess {Object} error An empty object with errors
    * @apiSuccess {Object} meta Object for additional information
    *
    * @apiSuccessExample Success-Response:
    * HTTP 200 OK
    * {
	  *    "data": [
		*               {
		*                 "id": "589a98d58bf2a16a73c8214e",
		*                 "type": "Entry",
		*                 "attributes": {
		*                   "_id": "589a98d58bf2a16a73c8214e",
		*                   "__v": 0,
		*                   "date": "2017-02-08T04:04:37.325Z",
		*                   "message": "Error: Not found",
		*                   "status": "404",
		*                   "title": "Not found"
		*               },
    *               ...
		*   ]
    **/
    /**
      * @api {delete} https://localhost:3030/api/development/entries Remove entry
      * @apiName RemoveEntry
      * @apiGroup Entry
      * @apiVersion 0.1.0
      *
      * @apiDescription Function for removing document from MongoDB.
      *
      * @apiPermission All users
      *
      * @apiParam {String} id Entry id (send in body)
      *
      * @apiSuccess {Object} meta Object for additional information
      *
      * @apiSuccessExample Success-Response:
      * HTTP 200 OK
      * {
	    *  "meta": [
		  *           {
			*              "code": "200-6",
			*              "detail": "You have succesfully removed documents"
		  *           }
	    *         ]
      * }
      **/
      /**
        * @api {put} https://localhost:3030/api/development/entries/:id Update Entry
        * @apiName Update Entry
        * @apiGroup Entry
        * @apiVersion 0.1.0
        *
        * @apiDescription Function for update document fileds.
        *
        * @apiPermission All users
        *
        * @apiParam {String} id Entry id
        * @apiParam {Object} update To update entry request object must contains object update with updated fields & values.
        *
        * @apiSuccess {Object} meta Object for additional information
        *
        * @apiSuccessExample Success-Response:
        * HTTP 200 OK
        * {
	      *    "data": {
		    *        "type": "Entry",
		    *        "id": "589a98db538a826a8e2cb13e",
		    *        "attributes": {
        *     			"_id": "589a98db538a826a8e2cb13e",
        *     			"__v": 0,
        *     			"date": "2017-02-08T04:04:43.448Z",
        *     			"message": "Error: Not found",
        *     			"status": "404",
        *     			"title": "test"
        *     		}
        *     	}
        *    }
        **/
        /**
          * @api {get} https://localhost:3030/api/development/entries/:id Get Entry
          * @apiName Get Entry
          * @apiGroup Entry
          * @apiVersion 0.1.0
          *
          * @apiDescription Function for get  document fileds.
          *
          * @apiPermission All users
          *
          * @apiParam {String} id Entry id
          *
          * @apiSuccess {Object} meta Object for additional information
          *
          * @apiSuccessExample Success-Response:
          * HTTP 200 OK
          * {
  	      *    "data": {
  		    *        "type": "Entry",
  		    *        "id": "589a98db538a826a8e2cb13e",
  		    *        "attributes": {
          *     			"_id": "589a98db538a826a8e2cb13e",
          *     			"__v": 0,
          *     			"date": "2017-02-08T04:04:43.448Z",
          *     			"message": "Error: Not found",
          *     			"status": "404",
          *     			"title": "test"
          *     		}
          *     	}
          *    }
          **/
          /**
            * @api {post} https://localhost:3030/api/development/entries/:id Create Entry
            * @apiName Create Entry
            * @apiGroup Entry
            * @apiVersion 0.1.0
            *
            * @apiDescription Function for get  document fileds.
            *
            * @apiPermission All users
            *
            * @apiSuccess {Object} newObj Object with fields to save
            *
            * @apiSuccessExample Success-Response:
            * HTTP 200 OK
            * {
	          *  "meta": [
		        *           {
			      *            "code": "200-5",
			      *            "detail": "Document successuly saved!"
		        *           }
	          *    ]
            * }
            **/
