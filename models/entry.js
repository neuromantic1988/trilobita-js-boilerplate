/* jshint esversion:6 */
/*
 * Document schema for log entry
 */
const mongoose = require('mongoose'),
	    Entry = new mongoose.Schema({
		      title: {
			        type: String,
			        default: 'Undefined error'
		      },
		      status: {
			        type: String,
			        default: 'Undefined'
		      },
		      message: {
			        type: String,
			        default: 'Server respons undefined'
		      },
		      date: {
			        type: Date,
			        default: Date.now
		      }
	    });
/* Export module */
module.exports = mongoose.model('Entry', Entry);
