/* jshint esversion:6 */

/*
 * Set main variables
 */
const debug = require('debug')('axolotl-js:server'),
      /* All node/native modules */
      modules = require('./interfaces/modules'),
      /* Scrapyard - main objects JSON*/
      scrapyard = require('./interfaces/scrapyard'),
      /* Utils - helpers for work with any common
       * functions.  */
      utils = require('./interfaces/utils'),
      /* Settings for all */
      settings = require('./interfaces/settings'),
      /* Send in appication instance Object */
	    app = require('./app.js')({
          scrapyard: scrapyard
      });
	/*
	 * Normalize a port into a number,
   * string, or false.
	 */
const normalizePort = value => {
		const port = parseInt(value, 10);
		if(isNaN(port)) return value;
		if(port >= 0) return port;
		return '3030';
},
	/*
	 * Event listener for HTTP server "error" event.
	 */
      onError = (error) => {
			    if(error.syscall !== 'listen') {
              utils.logger.console.info(error);
					    throw error;
			    } else {
				      let bind = typeof app.port === 'string'
					            ? `Pipe ${app.port}`
					            : `Port ${app.port}`;
				      /* handle specific listen errors with friendly messages */
				      switch (error.code) {
				      case 'EACCES':
					        utils.logger.console.info(bind + ' requires elevated privileges');
					        process.exit(1);
					        break;
				      case 'EADDRINUSE':
					        utils.logger.console.info(bind + ' is already in use');
					        process.exit(1);
					        break;
				      default:
					        throw error;
				      }
		      }
	    },
      /*
       * Event listener for HTTP server when application normal work
       */
	    onListening = () => {
			    let addr = scrapyard.httpServer.address(),
				      bind = typeof addr === 'string'
			            ? 'pipe ' + addr
			            : 'port ' + addr.port;
			    debug(`Listening on ${bind}`);
	    };
/*
 * Set application port
 */
app.set('port', normalizePort(settings.server.port));
/*
 * Create servers: HTTP
 */
scrapyard.httpServer = modules.http.createServer(app);
/*
 * Add listener to port
 */
scrapyard.httpServer.listen(settings.server.port);
scrapyard.httpServer.on('error', onError);
scrapyard.httpServer.on('listening', onListening);
