define({ "api": [
  {
    "type": "post",
    "url": "https://localhost:3030/api/development/entries/:id",
    "title": "Create Entry",
    "name": "Create_Entry",
    "group": "Entry",
    "version": "0.1.0",
    "description": "<p>Function for get  document fileds.</p>",
    "permission": [
      {
        "name": "All users"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "newObj",
            "description": "<p>Object with fields to save</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n \"meta\": [\n          {\n           \"code\": \"200-5\",\n           \"detail\": \"Document successuly saved!\"\n          }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apidoc/template.js",
    "groupTitle": "Entry"
  },
  {
    "type": "get",
    "url": "https://localhost:3030/api/development/entries/:id",
    "title": "Get Entry",
    "name": "Get_Entry",
    "group": "Entry",
    "version": "0.1.0",
    "description": "<p>Function for get  document fileds.</p>",
    "permission": [
      {
        "name": "All users"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Entry id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Object for additional information</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n   \"data\": {\n       \"type\": \"Entry\",\n       \"id\": \"589a98db538a826a8e2cb13e\",\n       \"attributes\": {\n    \t\t\t\"_id\": \"589a98db538a826a8e2cb13e\",\n    \t\t\t\"__v\": 0,\n    \t\t\t\"date\": \"2017-02-08T04:04:43.448Z\",\n    \t\t\t\"message\": \"Error: Not found\",\n    \t\t\t\"status\": \"404\",\n    \t\t\t\"title\": \"test\"\n    \t\t}\n    \t}\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "apidoc/template.js",
    "groupTitle": "Entry"
  },
  {
    "type": "delete",
    "url": "https://localhost:3030/api/development/entries",
    "title": "Remove entry",
    "name": "RemoveEntry",
    "group": "Entry",
    "version": "0.1.0",
    "description": "<p>Function for removing document from MongoDB.</p>",
    "permission": [
      {
        "name": "All users"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Entry id (send in body)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Object for additional information</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n \"meta\": [\n          {\n             \"code\": \"200-6\",\n             \"detail\": \"You have succesfully removed documents\"\n          }\n        ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apidoc/template.js",
    "groupTitle": "Entry"
  },
  {
    "type": "put",
    "url": "https://localhost:3030/api/development/entries/:id",
    "title": "Update Entry",
    "name": "Update_Entry",
    "group": "Entry",
    "version": "0.1.0",
    "description": "<p>Function for update document fileds.</p>",
    "permission": [
      {
        "name": "All users"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Entry id</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update",
            "description": "<p>To update entry request object must contains object update with updated fields &amp; values.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Object for additional information</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n   \"data\": {\n       \"type\": \"Entry\",\n       \"id\": \"589a98db538a826a8e2cb13e\",\n       \"attributes\": {\n    \t\t\t\"_id\": \"589a98db538a826a8e2cb13e\",\n    \t\t\t\"__v\": 0,\n    \t\t\t\"date\": \"2017-02-08T04:04:43.448Z\",\n    \t\t\t\"message\": \"Error: Not found\",\n    \t\t\t\"status\": \"404\",\n    \t\t\t\"title\": \"test\"\n    \t\t}\n    \t}\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "apidoc/template.js",
    "groupTitle": "Entry"
  },
  {
    "type": "get",
    "url": "https://localhost:3030/about",
    "title": "About page",
    "name": "About",
    "group": "General",
    "version": "0.1.0",
    "description": "<p>Main page - in this page you can see all libraries, used in Trilobita-JS.</p>",
    "filename": "apidoc/template.js",
    "groupTitle": "General"
  },
  {
    "type": "get",
    "url": "https://localhost:3030/api/development/entries",
    "title": "Check health",
    "name": "Alive",
    "group": "General",
    "version": "0.1.0",
    "description": "<p>Function to get answer, if microservice is alive</p>",
    "permission": [
      {
        "name": "All users"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n   \"meta\": [\n              {\n                \"code\": \"200-11\",\n                \"detail\": \"This microservice is alive!\"\n              }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apidoc/template.js",
    "groupTitle": "General"
  },
  {
    "type": "get",
    "url": "https://localhost:3030/api/development/entries",
    "title": "Get entries",
    "name": "Entries",
    "group": "List_arrays",
    "version": "0.1.0",
    "description": "<p>Function to get all entries in server</p>",
    "permission": [
      {
        "name": "All users"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Object with resource params</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>Object for additional information</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n   \"data\": [\n              {\n                \"id\": \"589a98d58bf2a16a73c8214e\",\n                \"type\": \"Entry\",\n                \"attributes\": {\n                  \"_id\": \"589a98d58bf2a16a73c8214e\",\n                  \"__v\": 0,\n                  \"date\": \"2017-02-08T04:04:37.325Z\",\n                  \"message\": \"Error: Not found\",\n                  \"status\": \"404\",\n                  \"title\": \"Not found\"\n              },\n              ...\n  ]",
          "type": "json"
        }
      ]
    },
    "filename": "apidoc/template.js",
    "groupTitle": "List_arrays"
  }
] });
