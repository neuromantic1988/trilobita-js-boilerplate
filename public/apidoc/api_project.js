define({
  "name": "Trilobita-JS",
  "version": "0.1.0",
  "description": "Boilerplate project to Node.js, Express, Mongoose & Mocha to Rest API",
  "title": "TrilobitaJS: Boilerplate for REST API",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2017-03-29T11:07:32.158Z",
    "url": "http://apidocjs.com",
    "version": "0.17.5"
  }
});
