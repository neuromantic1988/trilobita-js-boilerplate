/* jshint esversion:6 */
const supertest = require('supertest'),
			should = require('should'),
			scrapyard = require('../interfaces/scrapyard')
			app = require('../app.js')({
					scrapyard: scrapyard
				}),
			server = supertest.agent(app);

// UNIT test begin
describe('#1 Sample test: check health test', () => {
		it('#1-1 Should return answer with health', done => {
				server
					.get('/api/development/alive')
					.expect(200)
					.end((error, res) => {
							if(error) {
								throw error;
							}
							should(res).have.property('body');
							should(res.body).have.property('meta');
							should(res.body.meta[0]).have.property('code');
							should(res.body.meta[0]).have.property('detail')
							done();
						});
			});
	});
// UNIT test end
