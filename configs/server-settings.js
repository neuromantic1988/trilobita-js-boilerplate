/* jshint esversion:6 */
module.exports = {
    /* Port to run http/https servers app*/
		port: '3030',
    /* Host application */
		host: '0.0.0.0'
	};
