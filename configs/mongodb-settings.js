/* jshint esversion:6 */
module.exports = {
    /* Database name in MongoDB server  */
    database: 'trilobita',
    /* Port of MongoDB server */
		port: process.env.MONGO_PORT_27017_TCP_PORT || '27017',
    /* Host of MongoDB server */
		host: process.env.MONGO_PORT_27017_TCP_ADDR || 'localhost',
    /* Need SSL-serteficate */
    ssl: false
};
