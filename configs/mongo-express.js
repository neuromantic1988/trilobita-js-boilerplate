/*jshint esversion: 6*/
let mongo,
    url = require('url'),
	MS = require('../configs/mongodb-settings'),
    meConfigMongodbServer = process.env.ME_CONFIG_MONGODB_SERVER ? process.env.ME_CONFIG_MONGODB_SERVER.split(',') : false,
	mongoPathWithUser = `mongodb://${MS.user}:${MS.password}@${MS.host}:${MS.port}/${MS.database}`,
	mongoPathWithoutUser = `mongodb://${MS.host}:${MS.port}/${MS.database}`,
	R = require('rambda');


if (typeof process.env.MONGODB_PORT === 'string') {
  let mongoConnection = url.parse(process.env.MONGODB_PORT);
  process.env.ME_CONFIG_MONGODB_SERVER  = mongoConnection.hostname;
  process.env.ME_CONFIG_MONGODB_PORT    = mongoConnection.port;
}

if (process.env.VCAP_SERVICES) {
  let dbLabel = 'mongodb-2.4',
        env = JSON.parse(process.env.VCAP_SERVICES);
  if (env[dbLabel]) {
    mongo = env[dbLabel][0].credentials;
  }
} else {
    mongo = {
        db:       MS.database || 'trilobita',
        host:     process.env.MONGO_PORT_27017_TCP_ADDR || MS.host ||'localhost',
        password: MS.password || '',
        port:     process.env.MONGO_PORT_27017_TCP_PORT || MS.port || 27017,
        ssl:      MS.ssl || false,
		username: MS.username || '',
        url:      ((R.prop('user', MS) !== undefined) ? mongoPathWithUser : mongoPathWithoutUser) || 'mongodb://localhost:27017/trilobita',
  };
}

module.exports = {
  mongodb: {
    server: (meConfigMongodbServer.length > 1 ? meConfigMongodbServer : meConfigMongodbServer[0]) || mongo.host,
    port:   process.env.ME_CONFIG_MONGODB_PORT    || mongo.port,
    ssl: process.env.ME_CONFIG_MONGODB_SSL || mongo.ssl,
    sslValidate: process.env.ME_CONFIG_MONGODB_SSLVALIDATE || true,
    sslCA:  [],
    autoReconnect: true,
    poolSize: 1,
    admin: process.env.ME_CONFIG_MONGODB_ENABLE_ADMIN ? process.env.ME_CONFIG_MONGODB_ENABLE_ADMIN.toLowerCase() === 'true' : false,
    auth: [
      {
        database: process.env.ME_CONFIG_MONGODB_AUTH_DATABASE || mongo.db,
        username: process.env.ME_CONFIG_MONGODB_AUTH_USERNAME || mongo.username,
        password: process.env.ME_CONFIG_MONGODB_AUTH_PASSWORD || mongo.password
      },
    ],
    adminUsername: process.env.ME_CONFIG_MONGODB_ADMINUSERNAME || MS.user || '',
    adminPassword: process.env.ME_CONFIG_MONGODB_ADMINPASSWORD || MS.password || '',
    whitelist: [],
    blacklist: []
  },

  site: {
    baseUrl: process.env.ME_CONFIG_SITE_BASEURL || '/',
    cookieKeyName: 'mongo-express',
    cookieSecret:     process.env.ME_CONFIG_SITE_COOKIESECRET   || 'cookiesecret',
    host:             process.env.VCAP_APP_HOST                 || 'localhost',
    port:             process.env.VCAP_APP_PORT                 ||  5000,
    requestSizeLimit: process.env.ME_CONFIG_REQUEST_SIZE        || '50mb',
    sessionSecret:    process.env.ME_CONFIG_SITE_SESSIONSECRET  || 'sessionsecret',
    sslCert:          process.env.ME_CONFIG_SITE_SSL_CRT_PATH   || '',
    sslEnabled:       process.env.ME_CONFIG_SITE_SSL_ENABLED    || false,
    sslKey:           process.env.ME_CONFIG_SITE_SSL_KEY_PATH   || ''
  },
  useBasicAuth: process.env.ME_CONFIG_BASICAUTH_USERNAME !== '',

  basicAuth: {
    username: process.env.ME_CONFIG_BASICAUTH_USERNAME || 'admin',
    password: process.env.ME_CONFIG_BASICAUTH_PASSWORD || 'pass'
  },

  options: {
    console: true,
    documentsPerPage: 10,
    editorTheme: process.env.ME_CONFIG_OPTIONS_EDITORTHEME || 'rubyblue',
    maxPropSize: (100 * 1000),  // default 100KB
    maxRowSize: (1000 * 1000),  // default 1MB
    cmdType: 'eval',
    subprocessTimeout: 300,
    readOnly: false,
    collapsibleJSON: true,
    collapsibleJSONDefaultUnfold: 1,
    gridFSEnabled: false
  },

  defaultKeyNames: {

  }
};
